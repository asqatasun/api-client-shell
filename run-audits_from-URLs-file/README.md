# Run audits from a URLs file

## Warning

This POC (proof of concept) works only with Asqatasun API installed locally (`http://localhost:8081`).

- You can modify the script to fit your situation (domain, protocol, port, asqatasun user and password).
- A future version will allow to configure these parameters very easily.

## Documentation

```bash
usage: ./asqatasun_run-audits_from-URLs-file.sh --file <filePath> [ --wait      <seconds>   ]
                                                                  [ --tagPrefix <tagPrefix> ]
                                                                  [ --tag       <tagName>   ]   

      --file       <filePath>   MANDATORY - Path of URLs file (one URL per line)
      --wait       <seconds>    Waiting time between 2 calls to API    Default: '20' seconds
      --tagPrefix  <tagPrefix>  Prefix of Asqatasun tag                Default: '<fileName>'
      --tag        <tagName>    Asqatasun tag to identify audits       Default: '<tagPrefix>_2022.05.23-15h13'
```

## Usage

1. Start an Asqatasun API server (see following example: "Start an Asqatasun from Docker repository")
2. Run audits from a URLs file

### 1. Start an Asqatasun from Docker repository

see:
[Launch Asqatasun 5-SNAPSHOT API + Webapp](https://gitlab.com/asqatasun/asqatasun-docker/-/tree/main/5.x/5-SNAPSHOT/all-5-SNAPSHOT_ubuntu-18.04)

```bash
# Get Asqatasun Docker files
git clone git@gitlab.com:asqatasun/asqatasun-docker.git
mv asqatasun-docker git.asqatasun-docker
cd git.asqatasun-docker

# Option 1 - Go to Asqatasun SNAPSHOT directory (API and Webapp)
cd 5.x/5-SNAPSHOT/all-5-SNAPSHOT_ubuntu-18.04/

# Option 2 - Go to Asqatasun SNAPSHOT directory (API only)
cd 5.x/5-SNAPSHOT/api-5-SNAPSHOT_ubuntu-18.04/

    # Optional - You can limit access to Asqatasun: localhost only.
    cp -v .env.dist .env
    vim .env  # replace                  by
              #    API_HOST_IP=0.0.0.0   API_HOST_IP=127.0.0.1
              #    APP_HOST_IP=0.0.0.0   APP_HOST_IP=127.0.0.1  
              #    DB_HOST_IP=0.0.0.0    DB_HOST_IP=127.0.0.1

# build Docker image + launch Asqatasun and a new database
docker-compose build --no-cache
docker-compose up # Use [CTRL]+[C] to stop Asqatsun
```

If you want to restart Asqatasun with same database,
you can use the following command lines:

```bash
# Launch Asqatasun and same database
docker-compose rm -fsv
docker-compose up # Use [CTRL]+[C] to stop Asqatsun
```

If you want to restart Asqatasun with a new database,
you can use the following command lines:

```bash
# Launch Asqatasun and a new database
docker-compose rm -fsv
docker-compose up # Use [CTRL] [C] to stop Asqatsun
```

### 2. Run audits from a URLs file

see: [Example files of URLs](data/)

```bash
# Run audits from a URLs file
./asqatasun_run-audits_from-URLs-file.sh  --file data/websites_05-Urls.txt
```

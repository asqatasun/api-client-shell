#!/bin/bash
# shellcheck disable=SC2001
#            see: https://github.com/koalaman/shellcheck/wiki/SC2001

set -o errexit
#################################################################################
DEFAUTL_ASQA_USER="admin%40asqatasun.org"
DEFAUTL_ASQA_PASSWORD="myAsqaPassword"
DEFAUTL_ASQA_PROJECT_ID="1"
DEFAUTL_PROTOCOL="http"
DEFAUTL_HOST="localhost"
DEFAUTL_PORT="8081"
DEFAUTL_REFERENTIAL="RGAA_4_0"
DEFAUTL_LEVEL="AA"
DEFAUTL_WAITING_TIME=20
#################################################################################

TEMP=$(getopt -o cpt --long file:,tag:,tagPrefix:,wait: \
    -n 'javawrap' -- "$@")

    # SC2181: Check exit code directly with e.g. 'if mycmd;', not indirectly with $?.
    # For more information: https://www.shellcheck.net/wiki/SC2181
#    if [[ $? != 0 ]]; then
#        echo "Terminating..." >&2
#        exit 1
#    fi

TIME=$(date +%Hh%M)
DATE=$(date +%Y.%m.%d) 
usage() {
    echo "usage: ${0} --file <filePath>"
    echo ""
    echo "  --file       <filePath>   MANDATORY - Path of URLs file (one URL per line)"
    echo "  --wait       <seconds>    Waiting time between 2 calls to API    Default: '20' seconds"
    echo "  --tagPrefix  <tagPrefix>  Prefix of Asqatasun tag                Default: '<fileName>'"
    echo "  --tag        <tagName>    Asqatasun tag to identify audits       Default: '<tagPrefix>_${DATE}-${TIME}'"
    echo ""
    exit 2
}

# Note the quotes around $TEMP: they are essential!
eval set -- "$TEMP"

declare FILE_PATH=
while true; do
    case "$1" in
    --file)
        FILE_PATH="$2"
        shift 2
        ;;
    --tagPrefix)
        TAG_PREFIX="$2"
        shift 2
        ;;
    --tag)
        TAG_NAME="$2"
        shift 2
        ;;
    --wait)
        WAITING_TIME="$2"
        shift 2
        ;;
    --)
        shift
        break
        ;;
    *) break ;;
    esac
done


###### TODO : allow to customise following parameter ##############################################################
ASQA_USER="${DEFAUTL_ASQA_USER}"
ASQA_PASSWORD="${DEFAUTL_ASQA_PASSWORD}"
ASQA_PROJECT_ID="${DEFAUTL_ASQA_PROJECT_ID}"
PROTOCOL="${DEFAUTL_PROTOCOL}"
HOST="${DEFAUTL_HOST}"
PORT="${DEFAUTL_PORT}"
REFERENTIAL="${DEFAUTL_REFERENTIAL}"
LEVEL="${DEFAUTL_LEVEL}"
#################################################################################
API_PREFIX_URL="${PROTOCOL}://${ASQA_USER}:${ASQA_PASSWORD}@${HOST}:${PORT}"
API_PREFIX_URL_SAFE="${PROTOCOL}://${HOST}:${PORT}"
# API_URL="${API_PREFIX_URL}/api/v1/audit/run"
API_URL="${API_PREFIX_URL}/api/v0/audit/page/run"
#################################################################################

# Check mandatory options
if [[ "${FILE_PATH}" == "" ]]; then
    echo ''
    echo 'Mandatory option is missing'
    echo ''
    usage
fi

# Check existence of the file
if [ ! -f  "${FILE_PATH}" ]; then
    echo ''
    echo "File does not exist: [ ${FILE_PATH} ]"
    echo ''
    usage
fi
FILE_NAME=$(basename -- "$FILE_PATH")

# Tag prefix option
if [[ "${TAG_PREFIX}" == "" ]]; then    
    TAG_PREFIX="${FILE_NAME}"
fi

# Tag name option
if [[ "${TAG_NAME}" == "" ]]; then
    TAG_NAME="${TAG_PREFIX}_${DATE}-${TIME}"
fi

# Waiting time between 2 calls to API
if [[ "${WAITING_TIME}" == "" ]]; then
    WAITING_TIME="${DEFAUTL_WAITING_TIME}"
fi



#########################################
# Create a new version
#########################################


function call_api() {
    # shellcheck disable=SC2001
    URL_TO_AUDIT=$(echo "${1}" | sed -e 's/^[[:space:]]*//')  # trim spaces
    curl -X POST \
         "${API_URL}"                                               \
         -H  "accept: */*"                                          \
         -H  "Content-Type: application/json"                       \
         -d "{                                                      \
                \"urls\": [    \"${URL_TO_AUDIT}\"  ],              \
                               \"referential\": \"${REFERENTIAL}\", \
                               \"level\": \"${LEVEL}\",             \
                               \"contractId\": ${ASQA_PROJECT_ID},       \
                               \"tags\": [\"${TAG_NAME}\"]          \
             }"
    ((API_CALL_NUMBER+=1))
    echo " <--- Audit ID | Line : ${API_CALL_NUMBER} - ${URL_TO_AUDIT}"
}

mkdir -p "audit_results/"
RESULT_PATH="audit_results/${TAG_NAME}.json"
RESULT_REQUEST="${API_PREFIX_URL}/api/v0/audit/tags/${TAG_NAME}"
RESULT_REQUEST_SAFE="${API_PREFIX_URL_SAFE}/api/v0/audit/tags/${TAG_NAME}"
function get_audit_results () {
    curl -s -X GET "${RESULT_REQUEST}" -o "${RESULT_PATH}"
}

# TODO  count "status" and compare to API_CALL_NUMBER
# TODO Checks all audit status: PROCESSING, ...
# see: https://gitlab.com/asqatasun/Asqatasun/-/blob/master/engine/asqatasun-api/src/main/java/org/asqatasun/entity/audit/AuditStatus.java
#                ANALYSIS,
#                       COMPLETED,
#                CONSOLIDATION,
#                CONTENT_ADAPTING,
#                       CONTENT_LOADING,
#                CRAWLING,
#                       SCENARIO_LOADING,
#                ERROR,
#                       INITIALISATION,
#                PENDING,
#                PROCESSING,
#                MANUAL_INITIALIZING,
#                MANUAL_ANALYSE_IN_PROGRESS,
#                MANUAL_COMPLETED
function check_audit_results () {
        grep "COMPLETED"        "${RESULT_PATH}" &>/dev/null
        RESULT_CHECH_0=$?
        grep "SCENARIO_LOADING" "${RESULT_PATH}" &>/dev/null
        RESULT_CHECH_1=$?
        grep "CONTENT_LOADING"  "${RESULT_PATH}" &>/dev/null
        RESULT_CHECH_2=$?
        grep "INITIALISATION"   "${RESULT_PATH}" &>/dev/null
        RESULT_CHECH_3=$?
#       echo "---> RESULT_CHECH_0 ${RESULT_CHECH_0}"
#       echo "---> RESULT_CHECH_1 ${RESULT_CHECH_1}"
#      echo "---> RESULT_CHECH_2 ${RESULT_CHECH_2}"
#       echo "---> RESULT_CHECH_3 ${RESULT_CHECH_3}"
       if [[ "${RESULT_CHECH_0}" == "1" ]]; then
            echo "unfinished"
       elif [[ "${RESULT_CHECH_1}" == "0" || "${RESULT_CHECH_2}" == "0" || "${RESULT_CHECH_3}" == "0"  ]]; then
            echo "unfinished"
       fi
}

# TODO Limit retry also with RETRY_NUMBER and API_CALL_NUMBER
function get_audit_results_for_tag () {
    echo "Try to get result --->  ${RESULT_REQUEST_SAFE}"
    get_audit_results
    CHECK=$(check_audit_results)
#    echo "DEBUG ---> ${CHECK}"
    RETRY_NUMBER=0
    while [[ "${CHECK}" == "unfinished" ]]
    do
        ((RETRY_NUMBER+=1))
        sleep 5
        echo "Retry ${RETRY_NUMBER} to get result --->  ${RESULT_REQUEST_SAFE}"
        get_audit_results
        if [ -f  "${RESULT_PATH}" ]; then
           CHECK=$(check_audit_results)
        fi
    done
    echo "----------------------------"
    echo "To see result: "
    echo "cat ${RESULT_PATH}"
    echo "cat ${RESULT_PATH} | jq"
}


function run_audits_from_URLs_file() {
    echo "--------------------------------------"
    echo "URL source file   : ${FILE_PATH}"
    echo "Asqatasun TAG     : ${TAG_NAME}"
    echo "Asqatasun results : ${RESULT_REQUEST_SAFE}"
    echo "Waiting time between 2 calls to API: ${WAITING_TIME} seconds"
    echo "--------------------------------------"
    ######################################
    API_CALL_NUMBER=0
    while read -r LINE
    do
        # shellcheck disable=SC2001
        URL=$(echo "${LINE}" | sed -e 's/^[[:space:]]*//') # trim spaces
        if [ "${URL}" != "" ]; then
            call_api "${URL}"
            sleep "${WAITING_TIME}"
        fi
    done < "${FILE_PATH}"
    ######################################
    echo "--------------------------------------"
    echo "URL source file   : ${FILE_PATH}"
    echo "Asqatasun TAG     : ${TAG_NAME}"
    echo "Asqatasun results : ${RESULT_REQUEST_SAFE}"
    echo "Waiting time between 2 calls to API: ${WAITING_TIME} seconds"
    echo "--------------------------------------"
}

##########################################
# Main tasks
##########################################
run_audits_from_URLs_file
get_audit_results_for_tag

exit 0
